import 'package:flutter/material.dart';
import 'models.dart';

class NotificationCard extends StatelessWidget {
  const NotificationCard({
    Key? key,
    required this.itemIndex,
    required this.notification,
  }) : super(key: key);

  final int itemIndex;
  final mNotification notification;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10.0,
        vertical: 5.0,
      ),
      // color: Colors.blueAccent,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Container(
            height: 70,
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 15),
                  blurRadius: 27,
                  color: Colors.black12, // Black color with 12% opacity
                ),
              ],
            ),
            child: Container(
              width: size.width,
              padding: const EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: const BoxDecoration(
                      color: Colors.amber,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    width: size.width - 50 - 20 - 40,
                    child: RichText(
                      text: TextSpan(
                          text: '',
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 10,
                              decoration: TextDecoration.none),
                          children: <TextSpan>[
                            TextSpan(
                                text: notification.user,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                            TextSpan(
                              text: ' ' + notification.description,
                            ),
                          ]),
                    ),
                  )
                ],
              ),
              margin: const EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(22),
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
