class mNotification {
  final int id;
  final String user, description;

  mNotification({
    required this.id,
    required this.user,
    required this.description,
  });
}

List<mNotification> notifications = [
  mNotification(
    id: 1,
    user: "Hồ Thị Mỹ Chi",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 2,
    user: "Hồ Thị Mỹ Chi 2",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 3,
    user: "Hồ Thị Mỹ Chi 3",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 4,
    user: "Hồ Thị Mỹ Chi 4",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 5,
    user: "Hồ Thị Mỹ Chi 5",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 6,
    user: "Hồ Thị Mỹ Chi 6",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
  mNotification(
    id: 7,
    user: "Hồ Thị Mỹ Chi 7",
    description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim",
  ),
];
